package com.nespresso.sofa.recruitment.restaurant.order;

import com.nespresso.sofa.recruitment.restaurant.Meal;
import com.nespresso.sofa.recruitment.restaurant.exceptions.UnavailableDishException;
import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;
import com.nespresso.sofa.recruitment.restaurant.stock.Stock;

import java.util.Map;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public abstract class Recipient {
    public static final String MOZZARELLA = "Mozzarella";
    public static final String TOMATOES = "tomatoes";
    public static final String OLIVE_OIL = "olive oil";

    private final Map<Ingredient, Quantity> neededIngredient;

    protected Recipient() {
        neededIngredient = applyNeededIngredient();
    }

    public abstract Map<Ingredient, Quantity> applyNeededIngredient();

    public abstract Recipient retreiveDish();

    public void prepare(Stock stock) {
        if(!stock.hasEnoughFor(neededIngredient)){
            throw new UnavailableDishException();
        }
    }
}
