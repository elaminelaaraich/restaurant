package com.nespresso.sofa.recruitment.restaurant.order;

import com.nespresso.sofa.recruitment.restaurant.DishCommande;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class DishCommandeParser {
    public static DishCommande parse(String recipe) {
        String[] infos = recipe.split(" ");
        int numberOfDish = Integer.parseInt(infos[0]);

        if(recipe.contains("Tomato Mozzarella Salad")) {
            Recipient recipient = new TomatoMozzarellaSalad();
            return new DishCommande(recipient, numberOfDish);
        } else if(recipe.contains("Pizza")){
            Recipient recipient = new Pizza();
            return new DishCommande(recipient, numberOfDish);
        }
        return null;
    }
}
