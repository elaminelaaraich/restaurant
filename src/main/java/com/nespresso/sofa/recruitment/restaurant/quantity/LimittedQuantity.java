package com.nespresso.sofa.recruitment.restaurant.quantity;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class LimittedQuantity implements Quantity {
    private int quantity;

    public LimittedQuantity(int quantity) {

        this.quantity = quantity;
    }

    @Override
    public boolean isCountable() {
        return true;
    }

    @Override
    public boolean isMoreThan(Quantity quantity) {
        return this.quantity > ((LimittedQuantity) quantity).quantity;
    }
}
