package com.nespresso.sofa.recruitment.restaurant.parsers;

import com.nespresso.sofa.recruitment.restaurant.quantity.IllimitedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.LimittedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class QuantityParser {
    public static Quantity parse(String currentElement) {
        String[] infos = currentElement.split(" ");
        String quantity = infos[0];
        if (isNumeric(quantity)) {
            int quantityValue = Integer.parseInt(quantity);
            return new LimittedQuantity(quantityValue);
        }
        return new IllimitedQuantity();
    }

    private static boolean isNumeric(String info) {
        Pattern compile = Pattern.compile("\\d+");
        Matcher matcher = compile.matcher(info);
        while (matcher.find()) {
            if (matcher.group().equals(info)) {
                return true;
            }
        }
        return false;
    }
}
