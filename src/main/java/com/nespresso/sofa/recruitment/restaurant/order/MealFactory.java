package com.nespresso.sofa.recruitment.restaurant.order;

import com.nespresso.sofa.recruitment.restaurant.Meal;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class MealFactory {
    public static Meal create(int servedDishes, int cookingDuration) {
        return new Meal(servedDishes,cookingDuration);
    }
}
