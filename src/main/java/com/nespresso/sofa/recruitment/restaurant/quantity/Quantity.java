package com.nespresso.sofa.recruitment.restaurant.quantity;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public interface Quantity {
    boolean isCountable();

    boolean isMoreThan(Quantity quantity);
}
