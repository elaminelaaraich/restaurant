package com.nespresso.sofa.recruitment.restaurant;

import com.nespresso.sofa.recruitment.restaurant.order.MealFactory;
import com.nespresso.sofa.recruitment.restaurant.order.Recipient;
import com.nespresso.sofa.recruitment.restaurant.stock.Stock;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class DishCommande {
    public static final int FIRST_DURATION = 6;
    public static final int SAME_PLAT_DURATION = 3;
    private Recipient recipient;
    private int numberOfDish;

    public DishCommande(Recipient recipient, int numberOfDish) {

        this.recipient = recipient;
        this.numberOfDish = numberOfDish;
    }

    public void prepare(Stock stock) {
        recipient.prepare(stock);
    }

    public Meal retreiveMeal() {
        int duration = FIRST_DURATION;
        Recipient ancienRecipient = this.recipient.retreiveDish();
        for (int index = 1; index < numberOfDish; index++) {
            duration = calculeDuration(duration, ancienRecipient);
        }
        return MealFactory.create(numberOfDish, duration);

    }

    private int calculeDuration(int duration, Recipient ancienRecipient) {
        Recipient recipient = this.recipient.retreiveDish();
        if(recipient.equals(ancienRecipient))
        {
            duration += SAME_PLAT_DURATION;
        }
        return duration;
    }
}
