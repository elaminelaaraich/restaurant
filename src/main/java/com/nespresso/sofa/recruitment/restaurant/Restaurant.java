package com.nespresso.sofa.recruitment.restaurant;

import com.nespresso.sofa.recruitment.restaurant.order.DishCommandeParser;
import com.nespresso.sofa.recruitment.restaurant.parsers.Parser;
import com.nespresso.sofa.recruitment.restaurant.stock.Stock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Restaurant {
    private final List<Commande> commandes;
    private final Stock stock;

    public Restaurant(String... elements) {
        stock = Parser.parse(elements);
        commandes = new ArrayList<>();
    }

    public Ticket order(String recipe) {
        Commande order = new Commande();
        commandes.add(order);
        int numeroTicket = commandes.indexOf(order);
        return new Ticket(numeroTicket, recipe);
    }

    public DishCommande createDish(String recipe) {
        DishCommande dishsCommande = DishCommandeParser.parse(recipe);
        prepareCommande(dishsCommande);
        return dishsCommande;
    }

    private void prepareCommande(DishCommande dishCommande) {
        dishCommande.prepare(stock);
    }

    public Meal retrieve(Ticket ticket) {
        Commande order = ticket.retreiveOrder(commandes);
        List<DishCommande> dishsCommande = ticket.createDishs(this);
        order.addDish(dishsCommande);
        return order.retreiveMeal();
    }
}
