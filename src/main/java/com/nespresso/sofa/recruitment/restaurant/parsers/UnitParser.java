package com.nespresso.sofa.recruitment.restaurant.parsers;

import com.nespresso.sofa.recruitment.restaurant.unit.UnitFactory;
import com.nespresso.sofa.recruitment.restaurant.unit.Countable;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class UnitParser {
    public static Countable parse(String currentElement) {
        String[] infos = currentElement.split(" ");
        if (infos.length > 2) {
            String unitValue = infos[1];
            return UnitFactory.create(unitValue);
        }
        return UnitFactory.createCountableUnit();
    }
}
