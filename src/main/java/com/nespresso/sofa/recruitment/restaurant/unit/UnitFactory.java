package com.nespresso.sofa.recruitment.restaurant.unit;

import com.nespresso.sofa.recruitment.restaurant.unit.Countable;
import com.nespresso.sofa.recruitment.restaurant.unit.CountableUnit;
import com.nespresso.sofa.recruitment.restaurant.unit.NoUnit;
import com.nespresso.sofa.recruitment.restaurant.unit.Unit;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class UnitFactory {
    public static Countable createCountableUnit() {
        return new CountableUnit();
    }

    public static Countable createNoUnit() {
        return new NoUnit();
    }

    public static Countable create(String unitValue) {
        return new Unit(unitValue);
    }
}
