package com.nespresso.sofa.recruitment.restaurant.parsers;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.ingredient.IngredientFactory;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class IngredientParser {
    public static Ingredient parse(String currentElement) {
        String[] infos = currentElement.split(" ");
        String ingredName = null ;
        try {
            ingredName = currentElement.substring(currentElement.indexOf(infos[2]));
        } catch (IndexOutOfBoundsException exception) {
            ingredName = infos[infos.length-1];
        }
        return IngredientFactory.create(ingredName);
    }
}
