package com.nespresso.sofa.recruitment.restaurant.stock;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;

import java.util.List;
import java.util.Map;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Stock {
    private final List<StockLevel> stockLevels;

    public Stock(List<StockLevel> stockLevels) {

        this.stockLevels = stockLevels;
    }

    public boolean hasEnoughFor(Map<Ingredient, Quantity> neededIngredient) {
        for (Ingredient currentIngredient : neededIngredient.keySet()) {
            Quantity quantity = neededIngredient.get(currentIngredient);
            if(!stockIsEnoughFor(currentIngredient, quantity)){
                return false;
            }
        }
        return true;
    }

    private boolean stockIsEnoughFor(Ingredient currentIngredient, Quantity quantity) {

        for (StockLevel currentStockLevel : stockLevels) {
            if(currentStockLevel.has(currentIngredient))
            {
               return hasEnoughQuantity(quantity, currentStockLevel);
            }
        }
        return true;
    }

    private boolean hasEnoughQuantity(Quantity quantity, StockLevel currentStockLevel) {
        if(!currentStockLevel.hasEnoughQuantity(quantity)){
            return false;
        }
        return true;
    }
}
