package com.nespresso.sofa.recruitment.restaurant.order;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.quantity.IllimitedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.LimittedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elaminelaaraich on 05/05/2016.
 */
public class Pizza extends Recipient {
/*
    1 ball of Mozzarella
    * 4 tomatoes
    * some olive oil
    * 100cl of water
    * 300g of Flour
    * sea salt*/
    @Override
    public Map<Ingredient, Quantity> applyNeededIngredient() {
        Map<Ingredient, Quantity> neededElements = new HashMap<>();
        neededElements.put(new Ingredient(MOZZARELLA), new LimittedQuantity(1));
        neededElements.put(new Ingredient(TOMATOES), new LimittedQuantity(2));
        neededElements.put(new Ingredient(OLIVE_OIL), new IllimitedQuantity());
        neededElements.put(new Ingredient("water"), new LimittedQuantity(100));
        neededElements.put(new Ingredient("Flour"), new IllimitedQuantity());
        neededElements.put(new Ingredient("sea salt"), new IllimitedQuantity());
        return neededElements;
    }

    @Override
    public Recipient retreiveDish() {
        return this;
    }
}
