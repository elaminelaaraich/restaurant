package com.nespresso.sofa.recruitment.restaurant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Ticket {
    private int numeroTicket;
    private final List<String> recipes;

    public Ticket(int numeroTicket, String recipe) {
        this.recipes = new ArrayList<>();
        this.numeroTicket = numeroTicket;
        this.recipes.add(recipe);
    }

    public Ticket and(String recipe) {
        this.recipes.add(recipe);
        return this;
    }

    public Commande retreiveOrder(List<Commande> dishCommandes) {
        return dishCommandes.get(numeroTicket);
    }

    public List<DishCommande> createDishs(Restaurant restaurant) {
        List<DishCommande> dishCommandes = new ArrayList<>();
        for (String currentRecipe : recipes) {
            restaurant.createDish(currentRecipe);
        }
        return dishCommandes;
    }
}
