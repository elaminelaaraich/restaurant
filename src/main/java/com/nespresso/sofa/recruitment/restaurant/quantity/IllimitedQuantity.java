package com.nespresso.sofa.recruitment.restaurant.quantity;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class IllimitedQuantity implements Quantity {
    @Override
    public boolean isCountable() {
        return false;
    }

    @Override
    public boolean isMoreThan(Quantity quantity) {
        return true;
    }
}
