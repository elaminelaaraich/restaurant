package com.nespresso.sofa.recruitment.restaurant.parsers;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;
import com.nespresso.sofa.recruitment.restaurant.stock.Stock;
import com.nespresso.sofa.recruitment.restaurant.stock.StockLevel;
import com.nespresso.sofa.recruitment.restaurant.unit.Countable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Parser {

    public static Stock parse(String[] elements) {
        List<StockLevel> stock = new ArrayList<>();
        for (String currentElement : elements) {
            StockLevel stockLevel = createStockLevel(currentElement);
            stock.add(stockLevel);
        }
        return new Stock(stock);
    }

    private static StockLevel createStockLevel(String currentElement) {
        Ingredient indredient = IngredientParser.parse(currentElement);
        Quantity quantity = QuantityParser.parse(currentElement);
        Countable unit = UnitParser.parse(currentElement);
        return new StockLevel()
                .withInfos(currentElement)
                .withUnit(unit)
                .to(indredient)
                .withQuantity(quantity)
                .build();
    }
}
