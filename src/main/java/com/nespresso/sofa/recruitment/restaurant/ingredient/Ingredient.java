package com.nespresso.sofa.recruitment.restaurant.ingredient;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Ingredient {
    private String name;

    public Ingredient(String ingredName) {

        name = ingredName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ingredient)) return false;

        Ingredient that = (Ingredient) o;

        return name.equals(that.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
