package com.nespresso.sofa.recruitment.restaurant;

import java.util.List;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Meal {
    private int servedDishes;
    private int cookingDuration;

    public Meal(int servedDishes, int cookingDuration) {

        this.servedDishes = servedDishes;
        this.cookingDuration = cookingDuration;
    }

    public Meal() {

    }

    public int servedDishes() {
        return servedDishes;
    }

    public String cookingDuration() {
        return Integer.toString(cookingDuration);
    }

    public void addSubMeals(List<DishCommande> dishes) {
        int servedDishes = 0;
        int cookingDuration = 0;
        for (DishCommande currentDish : dishes) {
            servedDishes += currentDish.retreiveMeal().servedDishes;
            cookingDuration += currentDish.retreiveMeal().cookingDuration;
        }
        this.servedDishes = servedDishes;
        this.cookingDuration = cookingDuration;
    }
}
