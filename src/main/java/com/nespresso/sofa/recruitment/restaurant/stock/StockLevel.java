package com.nespresso.sofa.recruitment.restaurant.stock;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.ingredient.IngredientFactory;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;
import com.nespresso.sofa.recruitment.restaurant.unit.Countable;
import com.nespresso.sofa.recruitment.restaurant.unit.UnitFactory;
import com.sun.deploy.util.StringUtils;

import java.util.Arrays;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class StockLevel {
    private StockLevelBuilder stockLevelBuilder;
    private Ingredient indredient;
    private Countable unit;
    private Quantity quantity;

    public StockLevel(Ingredient indredient, Countable unit, Quantity quantity) {

        this.indredient = indredient;
        this.unit = unit;
        this.quantity = quantity;
    }

    public StockLevel() {
    }

    public StockLevelBuilder withInfos(String currentElement) {
        String[] infos = currentElement.split(" ");
        stockLevelBuilder = new StockLevelBuilder(infos);
        return stockLevelBuilder;
    }



    public boolean has(Ingredient currentIngredient) {
        return currentIngredient.equals(indredient);
    }

    public boolean hasEnoughQuantity(Quantity quantity) {
        return this.quantity.isMoreThan(quantity);
    }

    public static class StockLevelBuilder {
        private String[] infos;
        private Quantity quantity;
        private Countable unit;
        private Ingredient indredient;

        public StockLevelBuilder(String[] infos) {
            this.infos = infos;
        }

        public StockLevelBuilder withQuantity(Quantity quantity) {
            this.quantity = quantity;
            if (!quantity.isCountable()) {
                this.indredient = IngredientFactory.create(StringUtils.join(Arrays.asList(infos), " "));
                this.unit = UnitFactory.createNoUnit();
            }
            return this;
        }

        public StockLevelBuilder withUnit(Countable unit) {
            this.unit = unit;
            return this;
        }

        public StockLevelBuilder to(Ingredient indredient) {
            this.indredient = indredient;
            return this;
        }

        public StockLevel build() {

            return new StockLevel(indredient, unit, quantity);
        }
    }
}
