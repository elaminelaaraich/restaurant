package com.nespresso.sofa.recruitment.restaurant.order;

import com.nespresso.sofa.recruitment.restaurant.Meal;
import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;
import com.nespresso.sofa.recruitment.restaurant.quantity.IllimitedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.LimittedQuantity;
import com.nespresso.sofa.recruitment.restaurant.quantity.Quantity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class TomatoMozzarellaSalad extends Recipient {

    @Override
    public Map<Ingredient, Quantity> applyNeededIngredient() {
        Map<Ingredient, Quantity> neededElements = new HashMap<>();
        neededElements.put(new Ingredient(MOZZARELLA), new LimittedQuantity(1));
        neededElements.put(new Ingredient(TOMATOES), new LimittedQuantity(2));
        neededElements.put(new Ingredient(OLIVE_OIL), new IllimitedQuantity());
        return neededElements;
    }

    @Override
    public Recipient retreiveDish() {
        return this;
    }
}
