package com.nespresso.sofa.recruitment.restaurant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class Commande {
    private final List<DishCommande> dishes;

    public Commande() {
        dishes = new ArrayList<>();
    }

    public void addDish(List<DishCommande> dishCommande) {
        dishes.addAll(dishCommande);

    }

    public Meal retreiveMeal() {
        Meal parentMeal = new Meal();

        parentMeal.addSubMeals(dishes);

        return parentMeal;
    }
}
