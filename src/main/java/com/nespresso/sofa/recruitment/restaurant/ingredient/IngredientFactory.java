package com.nespresso.sofa.recruitment.restaurant.ingredient;

import com.nespresso.sofa.recruitment.restaurant.ingredient.Ingredient;

/**
 * Created by elaminelaaraich on 04/05/2016.
 */
public class IngredientFactory {
    public static Ingredient create(String ingredientName) {
        return new Ingredient(ingredientName);
    }
}
